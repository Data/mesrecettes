const express = require("express");
const path = require("path");
const sqlite3 = require("sqlite3").verbose();

// Création du serveur Express
const app = express();

// Configuration du serveur
app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "views"));
app.use(express.static(path.join(__dirname, "public")));
app.use(express.urlencoded({ extended: false }));

// Connexion à la base de donnée SQlite
const db_name = path.join(__dirname, "data", "MesRecettes.db");
const db = new sqlite3.Database(db_name, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Connexion réussie à la base de données 'MesRecettes.db'");
});

// Création de la table Recettes (Recette_ID, Pays, Description, Matériel, Epices, Ingredients, Recette)
const csql_create = `CREATE TABLE IF NOT EXISTS Recettes (
  Recette_ID INTEGER PRIMARY KEY AUTOINCREMENT,
  Pays VARCHAR(100) NOT NULL,
  Description VARCHAR(100) NOT NULL,
  Materiel TEXT,
  Epices TEXT,
  Ingredients TEXT,
  Recette TEXT
);`;
db.run(csql_create, err => {
  if (err) {
    return console.error(err.message);
  }
  console.log("Création réussie de la table 'Recettes'");
  // Alimentation de la table
 const csql_insert = `INSERT INTO Recettes (Recette_ID, Pays, Description, Materiel, Epices, Ingredients, Recette) VALUES
 (1, 'Belgique', 'Compote de pommes', 'Poillon, Spatule, plance a découper', 'Canelle', 'Pommes, Beurre', 'Découper les pommes, les mettre dans le poellon avec le beurre et une pincée de canelle. Chauffer à feu doux 10 à 15 minutes en mélangeant'),
 (2, 'France', 'Omelette', 'Pœlle', 'Poivre, Sel, Curcuma, Basilic', 'Beurre, 6œufs', 'Casser et battre les œufs, saller, poivrer et ajouter le curcuma. Verser dans la pœlle une fois le beurre fondu. Uine fois cuit, servir dans des assiètes et déchirer le basilic dessus. Bon Apétit.'),
 (3, 'France', 'Caramel', 'Poellon', 'Sel', 'Sucre fin, Beurre salé', 'Mettre le sucre dans le poellon laisser fondre sans remuer, couper la cuisson une fois le caramel coloré comme vous aimez, ajouter le beurre, la fleur de sel remuer légèrement et verser sur un marbre. casser ou per puis déguster une fois refroidi. ');`;
  db.run(csql_insert, err => {
   if (err) {
      return console.error(err.message);
    }
    console.log("Alimentation réussie de la table 'Recettes'");
  });
});



// Démarrage du serveur
app.listen(3000, () => {
    console.log("Serveur démarré (http://localhost:3000/) !");
});

// GET /
app.get("/", (req, res) => {
  // res.send("Bonjour le monde...");
  res.render("index");
});

// GET /about
app.get("/about", (req, res) => {
  res.render("about");
});

// GET /Recettes
app.get("/recettes", (req, res) => {
  const csql = "SELECT * FROM Recettes ORDER BY Pays";
  db.all(csql, [], (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    res.render("recettes", { model: rows });
  });
});

//GET /voir
app.get("/voir/:id", (req, res)=> {
  const id = req.params.id;
  const csql = "SELECT * FROM Recettes WHERE Recette_ID = ?";
  db.get(csql, id, (err, row) => {
    if (err){
      return console.error(err.message);
    }
    res.render("voir", {model : row});
  });
});


// GET /ccreate
app.get("/ccreate", (req, res) => {
  res.render("ccreate", { model: {} });
});

// POST /ccreate
app.post("/ccreate", (req, res) => {
  const csql = "INSERT INTO Recettes (Pays, Description, Materiel, Epices, Ingredients, Recette ) VALUES (?, ?, ?, ?, ?, ?)";
  const recipe = [req.body.Pays, req.body.Description, req.body.Materiel, req.body.Epices, req.body.Ingredients, req.body.Recette];
  db.run(csql, recipe, err => {
    if (err) {
      return console.error(err.message);
    }
    res.redirect("/recettes");
  });
});

// GET /cedit/5
app.get("/edit/:id", (req, res) => {
  const id = req.params.id;
  const csql = "SELECT * FROM Recettes WHERE Recette_ID = ?";
  db.get(csql, id, (err, row) => {
    if (err) {
      return console.error(err.message);
    }
    res.render("edit", { model: row });
  });
});

// POST /edit/5
app.post("/edit/:id", (req, res) => {
  const id = req.params.id;
  const recipe = [req.body.Pays, req.body.Description, req.body.Materiel, req.body.Epices, req.body.Ingredients, req.body.Recette, id];
  const csql = "UPDATE Recettes SET Pays = ?, Description = ?, Materiel = ?, Epices = ?, Ingredients = ?, Recette = ? WHERE (Recette_ID = ?)";
  db.run(csql, recipe, err => {
    if (err) {
      return console.error(err.message);
    }
    res.redirect("/recettes");
  });
});

// GET /cdelete/5
app.get("/cdelete/:id", (req, res) => {
  const id = req.params.id;
  const csql = "SELECT * FROM Recettes WHERE Recette_ID = ?";
  db.get(csql, id, (err, row) => {
    if (err) {
      return console.error(err.message);
    }
    res.render("cdelete", { model: row });
  });
});

// POST /cdelete/5
app.post("/cdelete/:id", (req, res) => {
  const id = req.params.id;
  const csql = "DELETE FROM Recettes WHERE Recette_ID = ?";
  db.run(csql, id, err => {
    if (err) {
      return console.error(err.message);
    }
    res.redirect("/recettes");
  });
});
