# MesRecettes - Une Web-App avec Express et SQlite 3


inspiré du cours CRUD en 10 étapes là: https://github.com/michelc/AppTest

Ce dépôt correspond à l'application développée.

Le but de ce projet ultra simple est de développer une application NodeJS :

* Faire tourner un site web en local de recettes.
* Peu gourmand en ressources et light; sans LAMP ou Nginx
* Gérer la mise à jour d’une base de données SQlite3.

On trouve assez facilement des exemples avec des bases de données NOSQL, mais je
n'arrive pas à me faire à MongoDB. J'ai donc opté pour une base de données SQLite3. 

# INSTALLATION
Installer nodejs sur Manjaro:
Dans un terminal entrez:
sudo pacman -Suy nvm

echo 'source /usr/share/nvm/init-nvm.sh' >> ~/.bashrc
exec $SHELL

nvm install 18

Clôner le projet:
Dans un terminal entrez la commande suivante sans les guillemets:
"git clone https://framagit.org/Data/mesrecettes.git"

Une fois dans le dossier sur votre disque dur, assurez vous que nodejs et npm soient installés .
entrez ensuite dans un termina et dans le répertoire de l'app, la commande:
npm install ou npm i ou npm upgrade 
Le dossier node_modules est maintenant créé et contient le nécessaire.

# Exécution:
npm start
vous affichera:
Serveur démarré (http://localhost:3000/) !
Connexion réussie à la base de données 'MesRecettes.db'
Création réussie de la table 'Recettes'
SQLITE_CONSTRAINT: UNIQUE constraint failed: Recettes.Recette_ID

Lancez Firefox et entrez l'addresse http://127.0.0.1:3000
